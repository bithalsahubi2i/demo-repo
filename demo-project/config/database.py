from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from urllib.parse import quote

import config.config as configuration
settings = configuration.Settings()

SQLALCHEMY_DATABASE_URL = "sqlite:///./{}".format(settings.DB)

engine = create_engine(
    SQLALCHEMY_DATABASE_URL
    ,connect_args = {"check_same_thread":False}
    ,encoding='latin1', echo=True
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()