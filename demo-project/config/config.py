from pydantic import BaseSettings

class Settings(BaseSettings):
    app_name: str = "Demo App"
    mailbox_server: str = "localmail@dummygroup.com"
    items_per_page: int = 20

    ENVIRONMENT: str = "production"
    REDIS_ADDRESS: str = "localhost:0000"
    MEANING_OF_LIFE: str =42
    MY_VAR: str = 'Hello world'

    DB: str = 'item.db'
    DB_USER: str = 'root'
    DB_PASS: str = 'Bi2i@1234'
    
    class Config:
        env_file = ".env"
        env_file_encoding = 'utf-8'

