import config.database as database
import models
# importing schema
# , model and database defined
import entity.schemas as schemas

def create_item(db, request):
    new_item = models.Item(name=request.name, description=request.description, price=request.price, tax=request.tax, user_id=request.user_id)
    db.add(new_item)
    db.commit()
    db.refresh(new_item)
    return new_item

