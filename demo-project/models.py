from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import Float

import config.database as database

Base = database.Base

class User(Base):
    __tablename__ = "user"
    __table_args__ = {'sqlite_autoincrement': True}

    user_id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), index=True)

class Item(Base):
    __tablename__ = "item"
    __table_args__ = {'sqlite_autoincrement': True}

    item_id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), index=True)
    description = Column(String(255), index=True)
    price = Column(Float, index=True)
    tax = Column(Float, index=True)
    user_id = Column(Integer, ForeignKey('user.user_id'))
    user = relationship("User", back_populates = "item")

User.item = relationship("Item", order_by = Item.item_id, back_populates = "user")



class Student(Base):
    __tablename__ = "student"
    __table_args__ = {'sqlite_autoincrement': True}

    student_id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), index=True)

class Book(Base):
    __tablename__ = "book"
    __table_args__ = {'sqlite_autoincrement': True}

    book_id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), index=True)
    description = Column(String(255), index=True)
    price = Column(Float, index=True)
    tax = Column(Float, index=True)
    student_id = Column(Integer, ForeignKey('student.student_id'))
    student = relationship("Student", back_populates = "book")

Student.book = relationship("Book", order_by = Book.book_id, back_populates = "student")