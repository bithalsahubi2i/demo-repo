from typing import List
from typing import Optional
from pydantic import BaseModel


class User(BaseModel):
    # user_id: int
    name: str

class Item(BaseModel):
    # item_id: int
    name: str
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None
    user_id: int
