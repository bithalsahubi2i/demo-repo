from fastapi import APIRouter

router = APIRouter()

from typing import List, Optional
from fastapi import Depends, FastAPI

import config.database as database
import models
# importing schema, model and database defined
import entity.schemas as schemas
from sqlalchemy.orm import Session

import services.users as userservice

# session object for database connection
def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()

#user APIs
@router.post("/user")
async def create_user(request: schemas.User, db: Session = Depends(get_db)):
    user = models.User(name=request.name)
    db.add(user)
    db.commit()
    db.refresh(user)
    return {"message": "Successfully created", "user": user}

@router.post("/users")
async def create_user(users: List[schemas.User], db: Session = Depends(get_db)):
    user_models = map(lambda x: models.User(name=x.name), users)
    db.add_all(list(user_models))
    db.commit()
    return {"message": "Successfully created"}

@router.get("/user")
def get_all_users(db: Session = Depends(get_db)):
    users = db.query(models.User).all()
    return users

@router.get("/user/{user_id}/item")
def get_all_users(user_id, db: Session = Depends(get_db)):
    # result = db.query(models.Item).join(models.User).filter(models.User.user_id == user_id).all()
    result = userservice.get_users_by_id(db, user_id)
    return result