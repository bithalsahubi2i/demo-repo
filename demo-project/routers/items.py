from fastapi import APIRouter

router = APIRouter()

from typing import List, Optional
from fastapi import Depends, FastAPI

import config.database as database
import models
# importing schema, model and database defined
import entity.schemas as schemas
from sqlalchemy.orm import Session

import services.items as itemservice

# session object for database connection
def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()

# item APIs
@router.post("/items")
async def create_item_orm(request: schemas.Item, db: Session = Depends(get_db)):
    new_item = itemservice.create_item(db, request)
    return {"message": "Successfully created", "item": new_item}

@router.get("/items")
def get_items_db(db: Session = Depends(get_db)):
    items = db.query(models.Item).all()
    return items

@router.get("/items/{item_id}")
async def read_item(item_id):
    print(item_id)
    return {"item_id": item_id}
