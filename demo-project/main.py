from typing import List, Optional
from fastapi import Depends, FastAPI
from pydantic import BaseModel

import config.database as database
import models
# importing schema, model and database defined
import entity.schemas as schemas

# create all table objects based on the models defined
models.Base.metadata.create_all(bind=database.engine)

from sqlalchemy.orm import Session
from sqlalchemy.sql import select

import routers.users as users
import routers.items as items

app = FastAPI()

app.include_router(users.router)
app.include_router(items.router)

conn = database.engine.connect()

# session object for database connection
def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()

# API Routes
@app.get("/")
async def root():
    return {"message": "Foo Bar"}




###############
from sqlalchemy import text


@app.get("/rawquery")
def raw_query():
    t = "SELECT * FROM item"
    result = conn.execute(t).fetchall()
    return result


import os

env_var = os.getenv("ENVIR", "DEFAULT_VAL")
print(f"Envir set to {env_var}")

import config.config as configuration
settings = configuration.Settings()

print(settings.REDIS_ADDRESS)
from urllib.parse import quote
SQLALCHEMY_DATABASE_URL_MYSQL ="mysql+mysqldb://{}:{}@127.0.0.1:3306/test".format(settings.DB_USER, quote(settings.DB_PASS))
print(SQLALCHEMY_DATABASE_URL_MYSQL)